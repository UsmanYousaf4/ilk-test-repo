<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://213.122.166.85/LTMAPITest/ECommerce_WebServ.asmx?op=SelectCourseDetails",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\r\n  <soap12:Body>\r\n    <SelectCourseDetails xmlns=\"http://tempuri.org/ECommerce_WS/Methods\">\r\n      <psXMLParams>string</psXMLParams>\r\n    </SelectCourseDetails>\r\n  </soap12:Body>\r\n</soap12:Envelope>",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: text/xml",
    "postman-token: a383f1ef-7c44-c839-013f-2ab9e890b1ee"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
?>