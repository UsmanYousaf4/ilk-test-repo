  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">                      
                <div class="footer-info">                 
                  <p> 
				  Join our mailing list to keep up-to-date with our<br>
				  latest projects and investment opportunities.
                  </p>
				</div>
				
				<div class="footer-submit">  
				  <form class="example" action="/action_page.php">
				  <input type="text" placeholder="Search.." name="search">
				  <button type="submit"><i class="">Submit</i></button>
				  </form>
				  </div> 

                <div class="footer-contact-info">  
                
                 <h4>Stay Connected</h4>
				 <p>19 Ashington Field Court, <br> Alesbury, OX26 3PQ</p>
				 <p><span>Telephone.</span> 0203 775 3421</p>
				 <p><span>Email.</span> contact@Tombolagroup.com</p>
                 <p><i class="fa fa-twitter fa-lg"></i> | <i class="fa fa-linkedin fa-lg"></i></p>	 
				</div>				  
            </div>
            
		<div class="divider"></div>		
						
          <div class="col-lg-6">       
              <div class="links">
              <h4>Navigate</h4>
			  <ul>
			  <li>Home</li>
			  <li>Projects</li>
			  <li>Products</li>
			  </ul>
			  
			  <ul>
			  <li>About</li>
			  <li>Contact</li>			 
			  </ul>
             
             <div id="policy">
			  <p><i style="padding-right:20px;">Privacy Policy</i> | <i style="padding-left:20px;">© Tombola 2018</i></p>
			 </div>				 
              </div>  
          </div>
        </div>
      </div>
    </div>    
  </footer>
  <!-- End  Footer -->