<?php

/**
*
* Template Name: functions.php
* Description: Add Features to wordpress theme
*
*/

add_theme_support( 'post-thumbnails');

/* Custom Post Type Start */
function create_posttype() {
register_post_type( 'portfolio',
// CPT Options
array(
  'labels' => array(
   'name' => __( 'portfolio' ),
   'singular_name' => __( 'Portfolio' )
  ),
  'public' => true,
  'has_archive' => false,
  'rewrite' => array('slug' => 'portfolio'),
 )
);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );
/* Custom Post Type End */


/*Custom Post type start*/
function cw_post_type_portfolio() {
$supports = array(
'title', // post title
'editor', // post content
'author', // post author
'thumbnail', // featured images
'excerpt', // post excerpt
'custom-fields', // custom fields
'comments', // post comments
'revisions', // post revisions
'post-formats', // post formats
);
$labels = array(
'name' => _x('portfolio', 'plural'),
'singular_name' => _x('portfolio', 'singular'),
'menu_name' => _x('Portfolio', 'admin menu'),
'name_admin_bar' => _x('Portfolio', 'admin bar'),
'add_new' => _x('Add New', 'add new'),
'add_new_item' => __('Add New portfolio'),
'new_item' => __('New portfolio'),
'edit_item' => __('Edit portfolio'),
'view_item' => __('View portfolio'),
'all_items' => __('All portfolio'),
'search_items' => __('Search portfolio'),
'not_found' => __('No portfolio found.'),
);
$args = array(
'supports' => $supports,
'labels' => $labels,
'public' => true,
'query_var' => true,
'rewrite' => array('slug' => 'portfolio'),
'has_archive' => true,
'hierarchical' => false,
);
register_post_type('portfolio', $args);
}
add_action('init', 'cw_post_type_portfolio');
/*Custom Post type end*/


//ACF Advanced Usage
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}

// Custom CSS Resource
function resources(){  	
 wp_enqueue_style('style', get_stylesheet_uri()); 
 wp_enqueue_style('bootstrap', get_template_directory_uri().'/assets/vendor/bootstrap/css/bootstrap.min.css');
}
add_action('wp_enqueue_scripts', 'resources');

// Logo Support
function theme_support(){
	add_theme_support('title-tag');	
	add_theme_support('custom-logo');	
}
add_action('after_setup_theme', 'theme_support');

// Fontawesome Support
function enqueue_load_fa() {
wp_enqueue_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_load_fa' );

// Menu Support
add_theme_support('menus');
// Regsiter Menu
function register_my_menu() {
  register_nav_menu('new-menu',__( 'Home Menu' ));
}
add_action( 'init', 'register_my_menu' );


?>
