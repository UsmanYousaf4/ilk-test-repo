<?php 
//get header.php file
get_header();
?>

<main id="main">

      <?php
                // Start the Loop.
               while (have_posts()) : the_post();
				   get_template_part('content', get_post_format()); ?>	   		
		<section id="about" class="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
			
				<img src="<?php the_post_thumbnail_url() ?>">
               <h3><?php the_title();?></h3>        
               <p><?php the_content(); ?></p>
            </div>
          </div>
        </div>
      </div>
    </section>	
		<?php   
				   
				endwhile;
            ?>
   
</main>

	<!--  cta partial Section -->
<?php get_template_part( 'partials/cta' ); ?>

</main>

<?php 
//get footer.php file
get_footer();
?>