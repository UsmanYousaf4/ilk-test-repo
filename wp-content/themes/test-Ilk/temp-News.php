<?php
/*Template Name: News*/
get_header();
query_posts(array(
   'post_type' => 'portfolio'
)); ?>


<main id="main">
<section id="about" class="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
               <h3></h3><h3>Our investment products have been designed to
accelerate the growth of our projects, while aiming
to provide outstanding returns for investors.</h3>        
               <p>Alongside our existing Environmental EIS and SEIS funds we also have
our Tombola Inheritance Tax Service, while we are preparing to launch an
Innovative Finance Individual Savings Accounts (IFISA). As soon as this is up and
running we’ll add all the relevant information here.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
<!-- ======= Projects Section ======= -->
	
    <section id="projects" class="">
        <div class="row"> 
		
		<?php
		while (have_posts()) : the_post(); ?>
        
		   <div class="col-lg-6" style="padding:0px;"> 
		   <div id="left" style="background-image:url('<?php the_post_thumbnail_url() ?>')"> 
		   <div class="intro-info">
          <h2><?php the_title(); ?></h2>
		  <?php the_excerpt(); ?>
           <a class="cta-btn align-middle" href="<?php the_permalink() ?>">Find out more </a>		  
           </div>
		   </div>
		   </div> 

		<?php endwhile; ?>
		
		  </div>     
    </section>
	<!-- End Projects Section -->

</main>


<?php
get_footer();
?>