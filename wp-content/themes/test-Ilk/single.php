<?php

get_header(); ?>

<main id="main">
        <div id="content" class="site-content" role="main">
		
		<h1>HELLO USMAN</h1>
           <?php
                // Start the Loop.
               while (have_posts()) : the_post();
				   get_template_part('content', get_post_format()); ?>
				   
				   <div class="post content">
				<h1 class="page-title"><a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h1>

				<div class="content">
					<?php the_content(); ?>
				</div>
			</div>
				<?php   
				   
				endwhile;
            ?>
        </div><!-- #content -->
</main>

<?php

get_footer();