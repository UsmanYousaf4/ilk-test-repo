
  <!-- ======= Banner Section ======= -->
  
  <?php $header = get_field('header-content'); ?>
  
  <section id="banner" class="clearfix" style="background-image:url('<?php echo $header['banner_image'];  ?>')" >
    <div class="container d-flex h-100" style="height: 120%!important">
      <div class="row justify-content-center align-self-center">
        <div class="intro-info">
          <h2><?php echo $header['title']; ?></h2>
		  <h4><?php echo $header['sub_title']; ?></h4>          
        </div>
      </div>

    </div>
  </section>
  <!-- End -->