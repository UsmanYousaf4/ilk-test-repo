<?php 
//get header.php file
get_header();
?>

<main id="main">

	<!--  cta partial Section -->
<?php get_template_part( 'partials/banner' ); ?>

    <!-- ======= About Section ======= -->	
	<?php $about = get_field('about-content'); ?>	
    <section id="about" class="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
               <h3><?php echo $about['about_text_1']; ?></h3>        
               <p><?php echo $about['about_text_2']; ?></p>
            </div>
          </div>
        </div>
      </div>
    </section>	
	<!-- End About Section -->
	


    <!-- ======= Projects Section ======= -->
	<?php $left = get_field('projects_left_content'); ?>
    <section id="projects" class="">
        <div class="row">         
		   <div class="col-lg-6" style="padding:0px;"> 
		   <div id="left" style="background-image:url('<?php echo $left['background_image'];  ?>')"> 
		   <div class="intro-info">
          <h2><?php echo $left['title']; ?></h2>
		  <p><?php echo $left['sub_title']; ?></p>
           <a class="cta-btn align-middle" href="<?php echo $left['button']; ?>">Find out more </a>		  
           </div>
		   </div>
		   </div>
		   
    <?php $right = get_field('projects_right_content'); ?>		   
		   <div class="col-lg-6" style="padding:0px;"> 
		   <div id="right" style="background-image:url('<?php echo $right['background_image'];  ?>')">
		   <div class="intro-info">
          <h2><?php echo $right['title']; ?> </h2>
		  <p><?php echo $right['sub_title']; ?></p>  
          <a class="cta-btn align-middle" href="<?php echo $right['button']; ?>">Find out more</a>		  
          </div> 		
		  </div>
		  </div>
		  </div>     
    </section>
	<!-- End Projects Section -->

  <!-- ======= About 2 Section ======= -->
  <?php $about2 = get_field('about_2_content'); ?>	
  
    <section id="about-2">
      <div class="container">
        <div class="row">		
          <div class="col-lg-3 text-center">
		  <i class="<?php echo $about2['icon1']; ?> fa-lg"></i> 
		  </div>
		  <div class="col-lg-3 text-center">
		  <i class="<?php echo $about2['Icon2']; ?> fa-lg"></i> 
		  </div>
		  <div class="col-lg-3 text-center">
		  <i class="<?php echo $about2['Icon3']; ?> fa-lg"></i> 
		  </div>
		  <div class="col-lg-3 text-center">
		  <i class="<?php echo $about2['Icon4']; ?> fa-lg"></i> 
		  </div>   		  
        </div>
		
		<div class="hr"></div>
		
		<div class="row">		
          <div class="col-lg-3 text-center"> 
		  <h4>Over</h4>
		 <h2><?php echo $about2['text_1']; ?></h2>
		 <p>Assets under management</p>
		  </div>
		  <div class="col-lg-3 text-center">
		  <h1><?php echo $about2['text_2']; ?></h1> 
		  <p>Jobs created</p>
		  </div>
		  <div class="col-lg-3 text-center">
		   <h1><?php echo $about2['text_3']; ?></h1>
		   <p>Tons of waste diverted from landfill</p>
		  </div>
		  <div class="col-lg-3 text-center">
		   <h1><?php echo $about2['text_4']; ?></h1>
		   <p>Successful planning permissions</p>
		  </div>  		  
        </div>
      </div>
    </section><!--  End About 2 Section -->
	
	
	<!--  cta partial Section -->
<?php get_template_part( 'partials/cta' ); ?>

</main>

<?php 
//get footer.php file
get_footer();
?>
  